var path = require('path')

module.exports = {
    context: __dirname,
    devtool: 'source-map',
    entry: {
        app: [ "./js/main" ]
    },
    resolve: {
        extensions: ["",  ".js", '.jsx' ]
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                }
            }
        ]
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: '[name].js',
        sourceMapFilename: "[file].map",
    },
    devServer: {
        inline: true,
        stats: {colors: true},
    }
}
