import React from 'react'

// Just because <blink> was removed from the HTML spec and removed from the
// browsers doesn't mean we have to give up on it! We can implement our own
// `<Blink>` React component.
export default class Blink extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: true
        }
    }

    componentWillMount() {
        this.timer = setInterval(
            // function bind syntax
            ::this.toggleState,
            500
        )
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }

    toggleState() {
        this.setState({ visible: !this.state.visible })
    }

    render() {
        const opacity = this.state.visible ? 100 : 0
        const style = {
            // shortcut for `opacity: opacity`
            opacity
        }
        return (
            <span style={style}>
                {this.props.children}
            </span>
        )
    }
}
