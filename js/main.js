import React from 'react'
import ReactDOM from 'react-dom'
import Blink from './components/blink'

class Hello extends React.Component {
    render() {
        // I'll often destructure things into the current scope. It isn't
        // important on this scale but it helps when you're destructuring many
        // properties.
        const { name } = this.props

        return (
            <div>
                Hello
                {/*
                    JSX strips the whitespace between Hello and <Blink> so
                    this is one way to restore it.
                */}
                {" "}
                <Blink>{name}</Blink>
                <hr />
                <blink>
                    This won't blink because the tag has been remvoed
                </blink>
            </div>
        )
    }
}

ReactDOM.render(<Hello name="Derek" />, document.getElementById('root'))
